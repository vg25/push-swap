/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: earruaba <earruaba@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/22 17:22:16 by earruaba          #+#    #+#             */
/*   Updated: 2021/09/23 03:34:00 by earruaba         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include "../libft/libft.h"

# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>

typedef struct s_list
{
	struct s_list	*previous;
	int				content;
	struct s_list	*next;
}			t_list;

typedef struct s_stack
{
	char			*str;
	struct s_stack	*next;
}			t_stack;

typedef struct s_data
{
	int	min;
	int	max;
	int	c_min;
	int	c_max;
	int	chunk;
}		t_data;

// ERROR
int		ft_error(char **strs, t_list **a, t_list **b, t_stack **stack);

// PARSER
char	**ft_split_arg(char **argv);
int		count_num(char *arg);
int		no_double(char **str, int size);
int		only_nbr(char **str, int size);
int		parser(char **str, int size);

// PRINT STACK
int		ft_ss(t_stack *lst);
int		ft_rr(t_stack *lst);
int		ft_rrr(t_stack *lst);
void	ft_lstprnt(t_stack *lst);

void	stack_clear(t_stack **satck);
void	ft_stack(char *str, t_stack **stack);

// LIST
int		lst_size(t_list *lst);
t_list	*lst_new(int content);
t_list	*lst_last(t_list *lst);
void	lst_addback(t_list **lst, t_list *new);
void	lst_addfront(t_list **lst, t_list *new);

void	lst_delone(t_list *lst, void (*del)(int));
void	lst_clear(t_list **lst, void (*del)(int));
void	lst_printf(t_list *lst);
int		lst_is_sort(t_list *lst);

void	ft_init_lst(t_list **a, char **str, int size);
t_data	init_data(void);

// OP
void	push_b(t_list **a, t_list **b, t_stack **stack);
void	push_a(t_list **b, t_list **a, t_stack **stack);
void	reverse_rra(t_list **a, t_stack **stack);
void	reverse_rrb(t_list **b, t_stack **stack);
void	reverse_rr(t_list **a, t_list **b, t_stack **stack);
void	rotate_a(t_list *a, t_stack **stack);
void	rotate_b(t_list *b, t_stack **stack);
void	rotate_rr(t_list *a, t_list *b, t_stack **stack);
void	swap_a(t_list *a, t_stack **stack);
void	swap_b(t_list *b, t_stack **stack);
void	ft_swap_ss(t_list *a, t_list *b, t_stack **stack);

// SORT

int		low(t_list *lst);
int		high(t_list *lst);

void	ft_threeb(t_list **b, t_stack **stack);
void	ft_three(t_list **a, int size, t_stack **stack);

void	ft_five(t_list **a, t_list **b, t_stack **stack, t_data *data);

int		chunk_min_five(int chunk, int min, int max);
int		chunk_max_five(int chunk, int min, int max);
int		chunk_min(int chunk, int min, int max);
int		chunk_max(int chunk, int min, int max);

int		ft_is_chunk(t_list *lst, int c_min, int c_max);
int		scan_next(t_list *lst, int c_min, int c_max);
int		scan_back(t_list *lst, int c_min, int c_max);
int		first_or_second(t_list *lst, int hold_first, int hold_second);
int		ft_middle(t_list *a, int hold);
void	rot_or_rev_a(t_list **a, int hold, t_stack **stack);
void	rot_or_rev_b(t_list **b, int hold, t_stack **stack);
int		fbefore_push(t_list **a, t_list **b, t_stack **stack, t_data *data);
int		before_push(t_list **a, t_list **b, t_stack **stack, t_data *data);

void	ft_hundred(t_list **a, t_list **b, t_stack **stack, t_data *data);
void	ft_fivehundred(t_list **a, t_list **b, t_stack **stack, t_data *data);

void	ft_free_all(t_list **a, t_list **b, t_stack **stack, char **str);

void	ft_global_sort(t_list **a, t_list **b, t_stack **stack, t_data *data);

#endif
