/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   op_reverse.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: earruaba <earruaba@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/12 16:28:57 by earruaba          #+#    #+#             */
/*   Updated: 2021/09/23 03:25:15 by earruaba         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	reverse_rra(t_list **a, t_stack **stack)
{
	t_list	*last;
	t_list	*tmp;
	t_list	*new;

	if (!a || !*a || !(*a)->next)
		return ;
	tmp = *a;
	last = lst_last(tmp);
	new = lst_new(last->content);
	lst_addfront(&tmp, new);
	while (tmp->next->next)
		tmp = tmp->next;
	free(tmp->next);
	tmp->next = NULL;
	*a = new;
	(*a)->previous = NULL;
	ft_stack("rra", stack);
}

void	reverse_rrb(t_list **b, t_stack **stack)
{
	t_list	*last;
	t_list	*tmp;
	t_list	*new;

	if (!b || !*b || !(*b)->next)
		return ;
	tmp = *b;
	last = lst_last(tmp);
	new = lst_new(last->content);
	lst_addfront(&tmp, new);
	while (tmp->next->next)
		tmp = tmp->next;
	free(tmp->next);
	tmp->next = NULL;
	*b = new;
	(*b)->previous = NULL;
	ft_stack("rrb", stack);
}

void	reverse_rr(t_list **a, t_list **b, t_stack **stack)
{
	reverse_rra(a, stack);
	reverse_rrb(b, stack);
}
