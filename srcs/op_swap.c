/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   op_swap.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: earruaba <earruaba@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/22 01:30:34 by earruaba          #+#    #+#             */
/*   Updated: 2021/09/23 03:17:59 by earruaba         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	swap_a(t_list *a, t_stack **stack)
{
	int	tmp;
	int	tmp2;

	tmp = a->content;
	tmp2 = a->next->content;
	a->content = tmp2;
	a->next->content = tmp;
	ft_stack("sa", stack);
}

void	swap_b(t_list *b, t_stack **stack)
{
	int	tmp;
	int	tmp2;

	tmp = b->content;
	tmp2 = b->next->content;
	b->content = tmp2;
	b->next->content = tmp;
	ft_stack("sb", stack);
}

void	ft_swap_ss(t_list *a, t_list *b, t_stack **stack)
{
	swap_a(a, stack);
	swap_b(b, stack);
}
