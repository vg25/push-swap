/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list2.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: earruaba <earruaba@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/12 12:17:17 by earruaba          #+#    #+#             */
/*   Updated: 2021/09/23 03:04:27 by earruaba         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	lst_delone(t_list *lst, void (*del)(int))
{
	if (lst != NULL)
	{
		if (del && lst->content)
			(del)(lst->content);
		free(lst);
		lst = NULL;
	}
}

void	lst_clear(t_list **lst, void (*del)(int))
{
	t_list	*current;
	t_list	*next;

	if (*lst)
	{
		current = *lst;
		while (current != NULL)
		{
			next = current->next;
			lst_delone(current, (del));
			current = next;
		}
		*lst = NULL;
	}
}

void	lst_printf(t_list *lst)
{
	if (!lst)
		ft_putstr("empty list\n");
	while (lst)
	{
		if (!lst->content)
			ft_putstr("[NULL] ");
		else
			ft_putnbr(lst->content);
		lst = lst->next;
	}
	ft_putstr("\n");
	return ;
}

int	lst_is_sort(t_list *lst)
{
	t_list	*last;

	last = lst_last(lst);
	while (lst != last)
	{
		if (lst->content < lst->next->content)
			lst = lst->next;
		else
			return (0);
	}
	return (1);
}
