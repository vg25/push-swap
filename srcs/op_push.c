/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   op_push.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: earruaba <earruaba@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/12 15:27:11 by earruaba          #+#    #+#             */
/*   Updated: 2021/09/23 03:23:35 by earruaba         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	push_a(t_list **b, t_list **a, t_stack **stack)
{
	t_list	*tmp;
	t_list	*first;
	t_list	*new;

	if (!*b)
		return ;
	first = *b;
	new = lst_new(first->content);
	lst_addfront(a, new);
	tmp = *b;
	first->previous = NULL;
	*b = first->next;
	free(tmp);
	ft_stack("pa", stack);
}

void	push_b(t_list **a, t_list **b, t_stack **stack)
{
	t_list	*tmp;
	t_list	*first;
	t_list	*new;

	if (!*a)
		return ;
	first = *a;
	new = lst_new(first->content);
	lst_addfront(b, new);
	tmp = *a;
	first->previous = NULL;
	*a = first->next;
	free(tmp);
	ft_stack("pb", stack);
}
