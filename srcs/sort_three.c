/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_three.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: earruaba <earruaba@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/14 11:25:42 by earruaba          #+#    #+#             */
/*   Updated: 2021/09/23 03:25:52 by earruaba         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void	ft_sthree(t_list **a, t_stack **stack)
{
	if ((*a)->content > (*a)->next->content
		&& (*a)->content < (*a)->next->next->content)
		swap_a(*a, stack);
	if ((*a)->content > (*a)->next->content
		&& (*a)->next->content > (*a)->next->next->content)
	{
		swap_a(*a, stack);
		reverse_rra(a, stack);
	}
	if ((*a)->content > (*a)->next->next->content
		&& (*a)->next->content < (*a)->next->next->content)
		rotate_a(*a, stack);
	if ((*a)->content < (*a)->next->content
		&& (*a)->next->content > (*a)->next->next->content
		&& (*a)->next->next->content > (*a)->content)
	{
		swap_a(*a, stack);
		rotate_a(*a, stack);
	}
	if ((*a)->content < (*a)->next->content
		&& (*a)->content > (*a)->next->next->content)
		reverse_rra(a, stack);
}

void	ft_threeb(t_list **b, t_stack **stack)
{
	if ((*b)->content > (*b)->next->content)
		swap_b(*b, stack);
}

void	ft_three(t_list **a, int size, t_stack **stack)
{
	if (size <= 2 && (*a)->content > (*a)->next->content)
		swap_a(*a, stack);
	else if (size > 2)
		ft_sthree(a, stack);
}
