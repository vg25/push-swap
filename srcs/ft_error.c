/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: earruaba <earruaba@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/12 16:55:24 by earruaba          #+#    #+#             */
/*   Updated: 2021/09/23 06:06:54 by earruaba         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	ft_free_all(t_list **a, t_list **b, t_stack **stack, char **str)
{
	free(str);
	lst_clear(a, NULL);
	lst_clear(b, NULL);
	stack_clear(stack);
}

int	ft_error(char **strs, t_list **a, t_list **b, t_stack **stack)
{
	ft_putstr_fd("Error\n", 2);
	ft_free_all(a, b, stack, strs);
	return (0);
}
