/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_five.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: earruaba <earruaba@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/14 14:24:20 by earruaba          #+#    #+#             */
/*   Updated: 2021/09/23 03:25:52 by earruaba         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void	ft_count1(t_list **a, t_stack **stack)
{
	int		c;
	int		d;
	t_list	*tmp;

	c = 0;
	d = 0;
	tmp = *a;
	while (tmp->content != low(*a))
	{
		tmp = tmp->next;
		c++;
	}
	tmp = lst_last(*a);
	while (tmp->content != low(*a))
	{
		tmp = tmp->previous;
		d++;
	}
	while ((*a)->content != low(*a))
	{
		if (c > d)
			reverse_rra(a, stack);
		else
			rotate_a(*a, stack);
	}
}

static void	ft_return(t_list **a, t_list **b, t_stack **stack, t_data *data)
{
	while (*b != NULL)
	{
		if ((*b)->content == data->min)
			push_a(b, a, stack);
		else
		{
			if ((*b)->content > high(*a))
				while ((*a)->content != low(*a))
					ft_count1(a, stack);
			else
				while ((*a)->content < (*b)->content)
					rotate_a(*a, stack);
			push_a(b, a, stack);
		}
	}
	while ((*a)->content != data->min)
		ft_count1(a, stack);
}

void	ft_five(t_list **a, t_list **b, t_stack **stack, t_data *data)
{
	data->min = low(*a);
	data->max = high(*a);
	if (lst_is_sort(*a) != 1)
	{
		push_b(a, b, stack);
		push_b(a, b, stack);
		ft_three(a, lst_size(*a), stack);
		ft_threeb(b, stack);
		ft_return(a, b, stack, data);
	}
}
