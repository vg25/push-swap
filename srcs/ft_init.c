/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: earruaba <earruaba@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/13 11:13:30 by earruaba          #+#    #+#             */
/*   Updated: 2021/09/23 03:01:36 by earruaba         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

t_data	init_data(void)
{
	t_data	data;

	data.min = 0;
	data.max = 0;
	data.c_min = 0;
	data.c_max = 0;
	data.chunk = 0;
	return (data);
}

void	ft_init_lst(t_list **a, char **str, int size)
{
	t_list	*new;
	int		i;

	i = 0;
	while (i < size)
	{
		new = lst_new(ft_atoi(str[i]));
		if (!new)
		{
			lst_clear(a, NULL);
			return ;
		}
		lst_addback(a, new);
		i++;
	}
}
