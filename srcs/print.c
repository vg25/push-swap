/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: earruaba <earruaba@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/12 11:20:58 by earruaba          #+#    #+#             */
/*   Updated: 2021/09/23 03:04:27 by earruaba         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	ft_ss(t_stack *lst)
{
	t_stack	*lst2;
	size_t	i;

	lst2 = lst->next;
	i = ft_strlen(lst->str);
	if (ft_strncmp(lst->str, "sa", i) == 0
		&& ft_strncmp(lst2->str, "sb", i) == 0)
		return (1);
	if (ft_strncmp(lst->str, "sb", i) == 0
		&& ft_strncmp(lst2->str, "sa", i) == 0)
		return (1);
	return (0);
}

int	ft_rr(t_stack *lst)
{
	t_stack	*lst2;
	size_t	i;

	lst2 = lst->next;
	i = ft_strlen(lst->str);
	if (ft_strncmp(lst->str, "ra", i) == 0
		&& ft_strncmp(lst2->str, "rb", i) == 0)
		return (1);
	if (ft_strncmp(lst->str, "rb", i) == 0
		&& ft_strncmp(lst2->str, "ra", i) == 0)
		return (1);
	return (0);
}

int	ft_rrr(t_stack *lst)
{
	t_stack	*lst2;
	size_t	i;

	lst2 = lst->next;
	i = ft_strlen(lst->str);
	if (ft_strncmp(lst->str, "rra", i) == 0
		&& ft_strncmp(lst2->str, "rrb", i) == 0)
		return (1);
	if (ft_strncmp(lst->str, "rrb", i) == 0
		&& ft_strncmp(lst2->str, "rra", i) == 0)
		return (1);
	return (0);
}

void	ft_lstprnt(t_stack *lst)
{
	int	n;

	if (!lst)
		return ;
	while (lst && lst->next)
	{
		n = 1;
		if (ft_ss(lst) == 1)
			ft_putstr("ss\n");
		else if (ft_rr(lst) == 1)
			ft_putstr("rr\n");
		else if (ft_rrr(lst) == 1)
			ft_putstr("rrr\n");
		else
		{
			n = 0;
			ft_putendl(lst->str);
		}
		lst = lst->next;
		if (n == 1)
			lst = lst->next;
	}
	if (lst->next == NULL)
		ft_putendl(lst->str);
}
