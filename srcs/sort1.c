/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort1.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: earruaba <earruaba@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/15 04:42:39 by earruaba          #+#    #+#             */
/*   Updated: 2021/09/23 02:59:21 by earruaba         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	chunk_min_five(int chunk, int min, int max)
{
	int	band;

	band = ((max - (min)) / 8);
	if (chunk == 1)
		return (min);
	if (chunk == 2)
		return (min + band);
	return (min + (band * (chunk - 1)));
}

int	chunk_max_five(int chunk, int min, int max)
{
	int	band;

	band = ((max - (min)) / 8);
	if (chunk == 1)
		return (min + band);
	if (chunk > 1 && chunk < 8)
		return (min + (band * chunk) - 1);
	return (max);
}

int	ft_is_chunk(t_list *lst, int c_min, int c_max)
{
	if (lst->content >= c_min && lst->content <= c_max)
		return (1);
	return (0);
}

int	scan_next(t_list *lst, int c_min, int c_max)
{
	while (lst != NULL)
	{
		if (ft_is_chunk(lst, c_min, c_max) == 1)
			return (lst->content);
		lst = lst->next;
	}
	return (0);
}

int	scan_back(t_list *lst, int c_min, int c_max)
{
	lst = lst_last(lst);
	while (lst != NULL)
	{
		if (ft_is_chunk(lst, c_min, c_max) == 1)
			return (lst->content);
		lst = lst->previous;
	}
	return (0);
}
