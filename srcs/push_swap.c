/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: earruaba <earruaba@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/10 08:42:42 by earruaba          #+#    #+#             */
/*   Updated: 2021/09/23 03:20:54 by earruaba         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static char	**new_tab(char **argv, int argc)
{
	int		i;
	int		j;
	char	**tab;

	i = 1;
	j = 0;
	tab = (char **)malloc(sizeof(char *) * argc - 1);
	while (i < argc - 1 || argv[i] != NULL)
	{
		tab[j] = argv[i];
		i++;
		j++;
	}
	return (tab);
}

int	main(int argc, char **argv)
{
	t_list	*a;
	t_list	*b;
	t_stack	*s;
	t_data	data;
	char	**tab;

	a = NULL;
	b = NULL;
	s = NULL;
	data = init_data();
	if (argc <= 1)
		return (0);
	tab = new_tab(argv, (argc));
	if (tab == NULL || parser(tab, (argc - 1)))
		return (ft_error(tab, &a, &b, &s));
	ft_init_lst(&a, tab, (argc - 1));
	if (lst_is_sort(a))
	{
		ft_free_all(&a, &b, &s, tab);
		return (0);
	}
	ft_global_sort(&a, &b, &s, &data);
	ft_lstprnt(s);
	ft_free_all(&a, &b, &s, tab);
	return (0);
}
