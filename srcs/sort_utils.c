/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_utils.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: earruaba <earruaba@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/13 02:28:59 by earruaba          #+#    #+#             */
/*   Updated: 2021/09/23 03:04:55 by earruaba         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	low(t_list *lst)
{
	int	small;

	small = lst->content;
	while (lst != NULL)
	{
		if (lst->content < small)
			small = lst->content;
		lst = lst->next;
	}
	return (small);
}

int	high(t_list *lst)
{
	int	big;

	big = lst->content;
	while (lst != NULL)
	{
		if (lst->content > big)
			big = lst->content;
		lst = lst->next;
	}
	return (big);
}
