/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   op_rotate.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: earruaba <earruaba@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/13 01:18:20 by earruaba          #+#    #+#             */
/*   Updated: 2021/09/23 03:25:52 by earruaba         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	rotate_a(t_list *a, t_stack **stack)
{
	t_list	*tmp;
	int		high;

	if (!a || !a->next)
		return ;
	high = a->content;
	tmp = a->next;
	while (a->next)
	{
		tmp = a->next;
		a->content = tmp->content;
		a = a->next;
	}
	a->content = high;
	a->next = NULL;
	ft_stack("ra", stack);
}

void	rotate_b(t_list *b, t_stack **stack)
{
	t_list	*tmp;
	int		high;

	if (!b || !b->next)
		return ;
	high = b->content;
	tmp = b->next;
	while (b->next)
	{
		tmp = b->next;
		b->content = tmp->content;
		b = b->next;
	}
	b->content = high;
	b->next = NULL;
	ft_stack("rb", stack);
}

void	rotate_rr(t_list *a, t_list *b, t_stack **stack)
{
	rotate_a(a, stack);
	rotate_b(b, stack);
}
