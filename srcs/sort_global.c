/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_global.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: earruaba <earruaba@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/16 12:59:27 by earruaba          #+#    #+#             */
/*   Updated: 2021/09/23 02:58:21 by earruaba         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	ft_global_sort(t_list **a, t_list **b, t_stack **stack, t_data *data)
{
	int	lstsize;

	lstsize = lst_size(*a);
	if (lstsize > 1 && lstsize <= 3)
		ft_three(a, lstsize, stack);
	if (lstsize > 3 && lstsize <= 5)
		ft_five(a, b, stack, data);
	if (lstsize > 5 && lstsize <= 100)
		ft_hundred(a, b, stack, data);
	if (lstsize > 100)
		ft_fivehundred(a, b, stack, data);
}
