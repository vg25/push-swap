/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: earruaba <earruaba@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/11 08:14:40 by earruaba          #+#    #+#             */
/*   Updated: 2021/09/23 03:22:22 by earruaba         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	no_double(char **str, int size)
{
	int	i;
	int	j;

	i = 0;
	while (i < size)
	{
		j = (i + 1);
		while (j < size)
		{
			if (ft_strcmp(str[i], str[j]) == 0)
				return (1);
			j++;
		}
		i++;
	}
	return (0);
}

int	only_nbr(char **str, int size)
{
	int	i;
	int	j;

	i = 0;
	while (i < size)
	{
		j = 0;
		while (str[i][j])
		{
			if (str[i][j] == '-' || str[i][j] == '+')
				j++;
			if (ft_isdigit(str[i][j]) == 0)
				return (1);
			j++;
		}
		i++;
	}
	return (0);
}

static int	int_min(char *str)
{
	int	i;

	i = 0;
	if (ft_strlen(str) >= 12)
		return (1);
	if (ft_strlen(str) == 11)
	{
		if (str[i] == '-')
			i++;
		if (ft_strcmp((str + i), "2147483648") > 0)
			return (1);
	}
	return (0);
}

static int	int_max(char *str)
{
	if (ft_strlen(str) > 10)
		return (1);
	if (ft_strlen(str) == 10)
	{
		if (ft_strcmp(str, "2147483647") > 0)
			return (1);
	}
	return (0);
}

int	parser(char **str, int size)
{
	int	i;
	int	is_big;

	i = 0;
	is_big = 0;
	if (only_nbr(str, size))
		return (1);
	if (no_double(str, size))
		return (1);
	while (i < size)
	{
		if (str[i][0] == '-')
			is_big = int_min(str[i]);
		else
			is_big = int_max(str[i]);
		if (is_big == 1)
			return (1);
		i++;
	}
	return (0);
}
