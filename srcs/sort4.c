/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort4.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: earruaba <earruaba@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/15 05:47:43 by earruaba          #+#    #+#             */
/*   Updated: 2021/09/23 03:16:45 by earruaba         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void	ft_repush(t_list **a, t_list **b, t_stack **stack, t_data *data)
{
	int	hold;

	if (data->chunk != 1)
		rot_or_rev_a(a, low(*a), stack);
	while (lst_size(*b) > 0)
	{
		hold = high(*b);
		rot_or_rev_b(b, hold, stack);
		push_a(b, a, stack);
	}
}

void	ft_hundred(t_list **a, t_list **b, t_stack **stack, t_data *data)
{
	int	before;

	data->chunk = 1;
	data->min = low(*a);
	data->max = high(*a);
	before = 1;
	while (data->chunk <= 5)
	{
		while (before == 1)
			before = before_push(a, b, stack, data);
		ft_repush(a, b, stack, data);
		data->chunk++;
		if (data->chunk > 5)
		{
			rot_or_rev_a(a, data->min, stack);
			return ;
		}
		before = before_push(a, b, stack, data);
	}
}

void	ft_fivehundred(t_list **a, t_list **b, t_stack **stack, t_data *data)
{
	int	before;

	data->chunk = 1;
	data->min = low(*a);
	data->max = high(*a);
	before = 1;
	while (data->chunk <= 8)
	{
		while (before == 1)
			before = fbefore_push(a, b, stack, data);
		ft_repush(a, b, stack, data);
		data->chunk++;
		if (data->chunk > 8)
		{
			rot_or_rev_a(a, data->min, stack);
			return ;
		}
		before = fbefore_push(a, b, stack, data);
	}
}
