# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: earruaba <earruaba@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2021/09/22 22:57:27 by earruaba          #+#    #+#              #
#    Updated: 2021/09/22 23:35:11 by earruaba         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME		=	push_swap

CC			=	gcc

CFLAGS		=	-Wall -Werror -Wextra

INC			=	-I inc/

LIBFT		=	libft/libft.a

SRCS_DIR	=	srcs

SRCS_FILES	=	ft_error.c \
				ft_init.c \
				list1.c \
				list2.c \
				op_push.c \
				op_reverse.c \
				op_rotate.c \
				op_swap.c \
				parser.c \
				print.c \
				push_swap.c \
				sort_five.c \
				sort_global.c \
				sort_three.c \
				sort_utils.c \
				sort1.c \
				sort2.c \
				sort3.c \
				sort4.c \
				stack.c 

SRCS		= $(addprefix $(SRCS_DIR)/, $(SRCS_FILES))

OBJS		= $(SRCS:.c=.o)

RM		= rm -f

$(NAME):	${OBJS}
		make -C libft
		${CC} ${OBJS} ${LIBFT} -o ${NAME}

%.o:		%.c
		${CC} ${CFLAGS} -c $< -o $@ $(INC)

all:		${NAME}

clean:
		${RM} ${OBJS}
		make clean -C libft

fclean:		clean
		${RM} ${NAME} ${LIBFT}

re:		fclean all

.PHONY:		all clean fclean re